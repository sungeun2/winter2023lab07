import java.util.Scanner;

public class TicTacToeGame {
	public static void main (String[] args) {
		//Scanner that allows user to enter the input
		Scanner sc = new Scanner(System.in);
		//create a board object
		Board board = new Board();
		//print welcome message
		System.out.println("Welcom to the Tic Tac Toe game!");
		System.out.println("Player 1's token: " + Square.X);
		System.out.println("Player 2's token: " + Square.O);
		
		//variables
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		
		//while loop that allows each player to play their turn and check if  there's a winner at the end
		//of each turn or if the board is full.
		while(!gameOver){
			//create a board object
			System.out.println(board);
			System.out.println("----------------------");
			//check with player's turn it is.
			if (player == 1) {
				playerToken = Square.X;
				System.out.println("Player" + player + " : it's your turn where do you want to place your token? Enter range between 0-2");
			}
			else {
				playerToken = Square.O;
				System.out.println("Player" + player + " : it's your turn where do you want to place your token? Enter range between 0-2");
	
			}
			//Use scanner to get inputs from user, the inputs represent row and column where the
			//user wants to place their token.
			System.out.println("row: ");
			int row = sc.nextInt();
			System.out.println("col: ");
			int col = sc.nextInt();
			//call placeToken method on the board, and ask the user to re-enter the input while
			//the playerToken method return false.
			while (!board.placeToken(row, col, playerToken)) {
				System.out.println("Invalid input, please re-enter your inputs");
				//Use scanner to get inputs from user, the inputs represent row and column where the
				//user wants to place their token.
				System.out.println("row: ");
				row = sc.nextInt();
				System.out.println("col: ");
				col = sc.nextInt();
			}
			//check if there is a winner or if the board is full.
			if (board.checkIfWinning(playerToken)) {
				System.out.println("Player " + player + " is the winner");
				System.out.println(board);
				gameOver = true;
			}
			else if (board.checkIfFull()) {
				System.out.println("It's a tie!");
				gameOver = true;
			}
			else {
				player = player + 1;
				if(player > 2) {
					player = 1;
				}
			}
			
		}
	}
}
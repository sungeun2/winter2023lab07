//Define an enum type Square
public enum Square {
	X,
	O,
	BLANK {
		//override method to print " _ " instead of BLANK
		public String toString() {
			return "_";
		}
	}
};

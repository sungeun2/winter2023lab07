public class Board {
	//private field that is type of Square[][]
	private Square[][] tictactoeBoard;
	//constructor
	public Board () {
		//initializes the Square[][] field to a 3 x 3 array
		this.tictactoeBoard = new Square[3][3];
		//tictactoeBoard[3][3] = Square.BLANK;
		for (int i = 0; i < this.tictactoeBoard.length; i++) {
			for (int j = 0; j < this.tictactoeBoard.length; j++) {
				this.tictactoeBoard[i][j] = Square.BLANK;
			}	
		}
	}
	//override method that returns a String representation of board
	public String toString() { 
		String emptyStr = " ";
		//nested loop where each row is printed on  a new line
		for (int i = 0; i < this.tictactoeBoard.length; i++) {
			for (int j = 0; j < this.tictactoeBoard[i].length; j++) {
				System.out.print(tictactoeBoard[i][j] + " ");
			}
			System.out.println(); // printing a new line
		}
		return emptyStr;
	}
	//instance method - placeToken, to place a token on the board
	public boolean placeToken(int row, int col, Square playerToken) {
		//check if row & col values are valid
		if (row < 0 || row >= 3 || col < 0 || col >= 3) {
			return false;
		}
		//check if the position of tictactoeBoard array has BLANK, then set the position to  the playerToken
		else if(tictactoeBoard[row][col] == Square.BLANK) {
				tictactoeBoard[row][col] = playerToken;
				return true;
		}else {
				return false;
		}
	}
	//instance method - checkIfFull, to check if the position of tictactoeBoard is full
	public boolean checkIfFull() {
		//nested loop to check every position of tictactoeBoard, is BLANK
		for (int i = 0; i < this.tictactoeBoard.length; i++) {
            for (int j = 0; j < this.tictactoeBoard[i].length; j++) {
				if(tictactoeBoard[i][j] == Square.BLANK) {
					return false;
				}
			}
		}
		return true;
	}
	//private method to  check if all the squares in a single row of the Square[][] array is equal to the playerToken.
	private boolean checkIfWinningHorizontal(Square playerToken) {
		//for loop to check every position of row
		for (int i = 0; i < tictactoeBoard.length; i++) {
			if (tictactoeBoard[i][0] == playerToken &&
				tictactoeBoard[i][1] == playerToken &&
				tictactoeBoard[i][2] == playerToken) {
				return true;
			}
		}
		return false;
	}
	//priavte method to check if all the squares in one column are equal to playerToken. 
	private boolean checkIfWinningVertical(Square playerToken) {
		//for loop to check every position of column
		for (int j = 0; j < tictactoeBoard.length; j++) {
			if (tictactoeBoard[0][j] == playerToken &&
				tictactoeBoard[1][j] == playerToken &&
				tictactoeBoard[2][j] == playerToken) {
					return true;
			}
		}
		return false;
	}
	//instance method - checkIfWinning using checkIfWinningHorizontal method and checkifWinningVertical method to check if a player win
	public boolean checkIfWinning(Square playerToken) {
		if (checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken)) {
			return true;
		}
		else{
			return false;
		}
	}
}
	
		
	
	
